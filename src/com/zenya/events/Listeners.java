package com.zenya.events;

import com.zenya.main.SGCraftAddon;
import com.zenya.utilities.SQLiteManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class Listeners implements Listener {
    SQLiteManager sqLiteManager = SGCraftAddon.sqLiteManager;

    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent event) {
        try {
            sqLiteManager.initQuestPoints(event.getPlayer(), 0);
        } catch(Exception e) {
            return;
        }
    }

    @EventHandler
    public void onAsyncPlayerChatEvent(AsyncPlayerChatEvent event) {
        if(event.getMessage().toUpperCase().equals("F")) {
            event.setCancelled(true);

            for(Player player : Bukkit.getServer().getOnlinePlayers()) {
                String message = ChatColor.translateAlternateColorCodes('&', "&8> &f" + event.getPlayer().getName() + " &7paid respect.");
                player.sendMessage(message);
            }
        }
    }
}
