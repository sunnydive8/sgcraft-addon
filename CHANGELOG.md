1.0.0:
- Initial release
- Added questpoints with SQLite DB

1.1.0:
- Added `/questpoints addall` subcommand
- Added questranks
- Added `sgcraftaddon.player` and `sgcraftaddon.admin` permission nodes
- Allowed players to pay respect [F]