package com.zenya.main;

import com.zenya.commands.QuestPoints;
import com.zenya.commands.QuestRanks;
import com.zenya.events.Listeners;
import com.zenya.utilities.ConfigManager;
import com.zenya.utilities.SQLiteManager;
import org.bukkit.plugin.java.JavaPlugin;

public class SGCraftAddon extends JavaPlugin {
    public static SGCraftAddon instance;
    public static SQLiteManager sqLiteManager;
    public static ConfigManager configManager;

    @Override
    public void onEnable() {
        instance = this;
        configManager = new ConfigManager(this);
        sqLiteManager = new SQLiteManager(this);

        this.getCommand("questpoints").setExecutor(new QuestPoints());
        this.getCommand("questranks").setExecutor(new QuestRanks());
        this.getServer().getPluginManager().registerEvents(new Listeners(), this);
    }

    @Override
    public void onDisable() {

    }
}
