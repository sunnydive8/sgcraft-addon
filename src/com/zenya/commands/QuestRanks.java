package com.zenya.commands;

import com.zenya.main.SGCraftAddon;
import com.zenya.utilities.ConfigManager;
import com.zenya.utilities.PointsToRanks;
import com.zenya.utilities.SQLiteManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class QuestRanks implements CommandExecutor {
    ConfigManager configManager = SGCraftAddon.configManager;
    SQLiteManager sqLiteManager = SGCraftAddon.sqLiteManager;
    PointsToRanks pointsToRanks = new PointsToRanks();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(args.length == 0) {
            // Send help
            sender.sendMessage(ChatColor.AQUA + "--- QuestRanks ---");
            sender.sendMessage(ChatColor.BLUE + "/questranks get <player> ");
            sender.sendMessage(ChatColor.BLUE + "/questranks list");
            sender.sendMessage(ChatColor.AQUA + "--- QuestRanks ---");
            sender.sendMessage(ChatColor.RED + "Made with " + ChatColor.DARK_RED + "<3 " + ChatColor.RED + "for SGCraft by Zenya");
            return true;
        }

        switch(args[0].toUpperCase()) {
            case "GET": {
                Player player;
                Integer questpoints;
                String rank;
                String display;

                if(args.length == 1) { //get

                    if(!(sender.hasPermission("questranks.get")) && !(sender.hasPermission("sgcraftaddon.player"))) {
                        sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use this command");
                        return true;
                    }

                    if(sender instanceof ConsoleCommandSender) {
                        sender.sendMessage(ChatColor.RED + "/questranks get [player]");
                        return true;
                    } else {
                        player = (Player) sender;
                    }

                } else { //get <player>
                    if(!(sender.hasPermission("questranks.get.other")) && !(sender.hasPermission("sgcraftaddon.player"))) {
                        sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use this command");
                        return true;
                    }

                    player = Bukkit.getServer().getPlayer(args[1]);

                    if(player == null) {
                        sender.sendMessage(ChatColor.DARK_RED + "That specified player is not online or does not exist");
                        return true;
                    }
                }

                // Get from DB
                try {
                    questpoints = sqLiteManager.getQuestPoints(player);
                } catch(Exception e) {
                    sender.sendMessage(ChatColor.DARK_RED + "That specified player does not exist in the database. Try getting them to relog");
                    return true;
                }

                // Convert
                rank = pointsToRanks.convert(questpoints);
                display = configManager.getRankDisplay(rank);

                sender.sendMessage(ChatColor.GOLD + player.getName() + " has the questrank: " + display);
                break;
            }

            case "LIST": {
                String rank;
                String display;
                Integer qpCost;

                if(!(sender.hasPermission("questranks.list")) && !(sender.hasPermission("sgcraftaddon.player"))) {
                    sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use this command");
                    return true;
                }

                ArrayList<String> ranks = new ArrayList<String>();
                ranks = configManager.getRanks();

                sender.sendMessage(ChatColor.AQUA + "--- QuestRanks ---");
                for(int i=ranks.size()-1; i>-1; i--) {
                    rank = ranks.get(i);
                    display = configManager.getRankDisplay(rank);
                    qpCost = configManager.getRankQP(rank);
                    sender.sendMessage(display + ChatColor.RESET + " - " + qpCost.toString() + " questpoints");
                }
                sender.sendMessage(ChatColor.AQUA + "--- QuestRanks ---");
                break;
            }

            default: {
                if(!(sender.hasPermission("questranks.help")) && !(sender.hasPermission("sgcraftaddon.player"))) {
                    sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use this command");
                    return true;
                }
                // Send help
                sender.sendMessage(ChatColor.AQUA + "--- QuestRanks ---");
                sender.sendMessage(ChatColor.BLUE + "/questranks get <player> ");
                sender.sendMessage(ChatColor.BLUE + "/questranks list");
                sender.sendMessage(ChatColor.AQUA + "--- QuestRanks ---");
                sender.sendMessage(ChatColor.RED + "Made with " + ChatColor.DARK_RED + "<3 " + ChatColor.RED + "for SGCraft by Zenya");
                break;
            }
        }
        return true;
    }
}
